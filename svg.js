class Calendar {
  /**
   * @param {String} selector
   * @param {Object} options
   * @param {Number} options.radius
   * @param {String} options.backgroundColor
   * @param {String} options.cursorColor
   */
  constructor ({ selector, options }) {
    this._angle = { a: 0, b: 0 }
    this._canvas = {}
    this._click = false
    this._cursorSelected = null
    this._DOMContainer = document.querySelector(selector)
    this._radius = options.radius
    this._sliders = []
    this._sliderNumber = 0
    this._zoneSelected = null

    this.initSizes()
    this.drawCanvas(options)
    this._canvas.element.addEventListener('mousemove', event => {
      if (this._click) {
        this._rect = this._canvas.element.getBoundingClientRect()
        this.zoneIdentification(event)
        this.calculateCoordinatesCursor()
        // this.calculateDataSlider()
      }
    })
    this._canvas.element.addEventListener('mouseup', event => {
      this._click = false
    })

    /*
    this._slider = this._canvas.querySelector('g path:nth-of-type(1)')
    this._cursorA = this._canvas.querySelector('g path:nth-of-type(2)')
    this._cursorB = this._canvas.querySelector('g path:nth-of-type(3)')
    */
  }

  /**
   * Init the different sizes to calculate the dimensions
   */
  initSizes () {
    this._canvas.backgroundSize = this._radius / 5
    this._canvas.margin = this._radius / 5
    this._canvas.width = (this._radius * 6) + (this._canvas.margin * 3)
    this._canvas.height = (this._radius * 2) + (this._canvas.margin * 3)

    this._canvas.zones = {}
    this._canvas.zones.a = {
      x1: 0,
      x2: this._canvas.width / 2,
      y1: 0,
      y2: this._canvas.height / 2,
    }
    this._canvas.zones.b = {
      x1: 0,
      x2: this._canvas.width,
      y1: this._canvas.height / 2,
      y2: this._canvas.height,
    }
    this._canvas.zones.c = {
      x1: this._canvas.width / 2,
      x2: this._canvas.width,
      y1: 0,
      y2: this._canvas.height / 2,
    }

    this._canvas.transformOrigin = {
      cx: this._canvas.margin + this._radius,
      cy: this._canvas.height / 2,
    }
  }

  /**
   * Create the SVG and the background path.
   *
   * @param {Object} options
   * @param {String} options.backgroundColor
   * @param {String} options.cursorColor
   */
  drawCanvas ({ backgroundColor, cursorColor }) {
    this._container = document.createElement('div')
    this._container.style.width = '100%'
    this._container.style.display = 'grid'
    this._container.style.gridTemplateColumns = '8fr 2fr'

    const button = document.createElement('button')
    button.innerText = 'Nouvel interval'
    button.addEventListener('click', () => this.addSlider(backgroundColor, cursorColor))
    const actions = document.createElement('div')
    actions.appendChild(button)

    this._canvas.element = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
    this._canvas.element.setAttribute('viewBox', `0 0 ${this._canvas.width} ${this._canvas.height}`)
    this._canvas.element.setAttribute('height', `${this._canvas.height}`)
    this._canvas.element.setAttribute('width', `${this._canvas.width}`)

    this._pathBackground = document.createElementNS('http://www.w3.org/2000/svg', 'path')
    this._pathBackground.setAttribute(
      'd',
      `M${this._canvas.margin},${this._canvas.height / 2} a${this._radius},${this._radius} 0, 0, 1 ${this._radius * 2},0 a${this._radius},${this._radius} 0, 0, 0 ${this._radius * 2},0 a${this._radius},${this._radius} 0, 0, 1 ${this._radius * 2},0`,
    )
    this._pathBackground.setAttribute('fill', 'transparent')
    this._pathBackground.setAttribute('stroke-width', this._canvas.backgroundSize)
    this._pathBackground.setAttribute('stroke', backgroundColor)

    this._canvas.element.appendChild(this._pathBackground)
    this._container.appendChild(this._canvas.element)
    this._container.appendChild(actions)
    this._DOMContainer.appendChild(this._container)
  }

  /**
   * Add a slider: 2 cursors and the background slider
   *
   * @param {String} backgroundColor
   * @param {String} cursorColor [white]
   * @param {String} cursorBorderColor [black]
   */
  addSlider (backgroundColor, cursorColor = 'white', cursorBorderColor = 'black') {
    const container = document.createElementNS('http://www.w3.org/2000/svg', 'g')
    container.classList.add(`slider__${++this._sliderNumber}`)

    const backgroundSlider = document.createElementNS('http://www.w3.org/2000/svg', 'path')
    backgroundSlider.setAttribute(
      'd',
      `M${this._canvas.margin},${this._canvas.height / 2} a${this._radius},${this._radius} 0, 0, 1 ${this._radius * 2},0 a${this._radius},${this._radius} 0, 0, 0 ${this._radius * 2},0 a${this._radius},${this._radius} 0, 0, 1 ${this._radius * 2},0`,
    )
    backgroundSlider.setAttribute('fill', 'transparent')
    backgroundSlider.setAttribute('stroke-width', this._canvas.backgroundSize)
    backgroundSlider.setAttribute('stroke', backgroundColor)

    const cursorWith = this._canvas.backgroundSize + (this._radius / 10)
    const cursorHeight = cursorWith / 3
    let cursorPositionInitial = {}
    cursorPositionInitial.x = this._canvas.margin - (cursorWith / 2)
    cursorPositionInitial.y = this._canvas.height / 2 - (cursorHeight / 2)

    const cursor1 = document.createElementNS('http://www.w3.org/2000/svg', 'path')
    cursor1.setAttribute('d', `M${cursorPositionInitial.x},${cursorPositionInitial.y} h${cursorWith} v${cursorHeight} h-${cursorWith}z`)
    cursor1.setAttribute('fill', cursorColor)
    cursor1.setAttribute('stroke', cursorBorderColor)
    cursor1.style.cursor = 'pointer'
    cursor1.addEventListener('mousedown', () => {
      this._click = true
      this._cursorSelected = {
        name: 'b',
        cursor: cursor1,
      }
    })

    const cursor2 = document.createElementNS('http://www.w3.org/2000/svg', 'path')
    cursor2.setAttribute('d', `M${cursorPositionInitial.x},${cursorPositionInitial.y} h${cursorWith} v${cursorHeight} h-${cursorWith}z`)
    cursor2.setAttribute('fill', cursorColor)
    cursor2.setAttribute('stroke', cursorBorderColor)
    cursor2.style.cursor = 'pointer'
    cursor2.addEventListener('mousedown', () => {
      this._click = true
      this._cursorSelected = {
        name: 'b',
        cursor: cursor2,
      }
    })

    container.appendChild(backgroundSlider)
    container.appendChild(cursor1)
    container.appendChild(cursor2)
    this._canvas.element.appendChild(container)
  }

  /**
   * Identify the zone selected
   *
   * @param {MouseEvent} event
   */
  zoneIdentification (event) {
    this._x = event.clientX - this._rect.left
    this._y = event.clientY - this._rect.top

    if (this._x > this._canvas.zones.a.x1 && this._x < this._canvas.zones.a.x2 &&
      this._y > this._canvas.zones.a.y1 && this._y < this._canvas.zones.a.y2) {
      this._zoneSelected = {
        cx: this._canvas.margin + this._radius,
        cy: this._canvas.height / 2,
        translate: `translate(0)`,
      }
    } else if (this._x > this._canvas.zones.b.x1 && this._x < this._canvas.zones.b.x2 &&
      this._y > this._canvas.zones.b.y1 && this._y < this._canvas.zones.b.y2) {
      this._zoneSelected = {
        cx: this._canvas.margin + (this._radius * 3),
        cy: this._canvas.height / 2,
        translate: `translate(${this._radius * 2})`,
      }
    } else if (this._x > this._canvas.zones.c.x1 && this._x < this._canvas.zones.c.x2 &&
      this._y > this._canvas.zones.c.y1 && this._y < this._canvas.zones.c.y2) {
      this._zoneSelected = {
        cx: this._canvas.margin + (this._radius * 5),
        cy: this._canvas.height / 2,
        translate: `translate(${this._radius * 4})`,
      }
    }
  }

  /**
   * Calculate the coordinates
   */
  calculateCoordinatesCursor () {
    const x = this._x - this._zoneSelected.cx
    const y = this._zoneSelected.cy - this._y
    console.log(this._x, this._zoneSelected.cx, y)
    /*this._angle[this._cursorSelected.name] = Math.sign(x) >= 0
                                             ? Math.atan(y / x)
                                             : Math.PI + Math.atan(y / x)

    this._cursorSelected.cursor.setAttribute(
      'transform',
      `${this._zoneSelected.translate} rotate(${180 - (this._angle[this._cursorSelected.name] * 180 / Math.PI)}, 30, 30)`,
    )*/

    const angle = Math.sign(x) >= 0
                  ? Math.atan(y / x)
                  : Math.PI + Math.atan(y / x)
    console.log(180 - (angle * 180 / Math.PI))

    this._cursorSelected.cursor.setAttribute(
      'transform',
      `${this._zoneSelected.translate} rotate(${180 - (angle * 180 / Math.PI)}, ${this._canvas.transformOrigin.cx}, ${this._canvas.transformOrigin.cy})`,
    )
  }

  calculateDataSlider () {
    const x = (this._angle.a - this._angle.b) * 25
    const y = (3 * Math.PI * 25) - x
    this._slider.setAttribute('stroke-dasharray', `${x} ${y}`)

    const z = (Math.PI - this._angle.a) * 25
    this._slider.setAttribute('stroke-dashoffset', `${-z}`)
  }
}
